
## Earthquake Box

This is a simple project for showing earthquakes on map. Right now it has two main feature:

1- Showing last week earthquakes in red bubbles

2- By clicking in every place of map it will return earthquakes with 3 and above magnitude in the 1000km surrounded area in blue bubbles 

## Usage

1- `npm install` or `yarn`

2- `npm start` or `yarn start`